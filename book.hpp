// book.hpp
#ifndef BOOK_H_
#define BOOK_H_

namespace boost {
    namespace serialization {
        class access;
    }
}

// simple class
class Book
{
    private:
        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
            ar & year;
            ar & pages;
        }
        int year;
        int pages;
    public:
    // serialization expects the object to have a default constructor
        Book (){};
        Book(int y, bool p) : year(y), pages(p){}

};

#endif