// Car.cpp
#include "Car.hpp"
#include <iostream>


int main(int argc, char *argv[])
{
    FuelType fuel = FuelType::DIESEL;

    if (fuel == FuelType::DIESEL)
    {
        std::cout << "tak działa enum class" << std::endl;
    }

    Wheels wheel = { "winter" };
    std::cout << wheel.type << std::endl;
}