#include "book.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <new>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>


static void show_usage(std::string name)
{
    std::cerr << "Usage: " << name << " <option>"
              << "\nOptions:\n"
              << "\t--help\t\tShow this help message\n"
              << "\ttxt\t\tsaving as text\n"
              << "\tbin\t\tsaving as binary data"
              << std::endl;
}


int main(int argc, char *argv[])
{
    const char* fileName = "mietek";

    // check the number of parameters
    if (argc < 2) {
        show_usage(argv[0]);
        return 1;
    }

    // create some objects
    const Book b1(1999, 100);

    for (int i = 1; i < argc; ++i) {
        std::string arg = argv[i];
        if (arg == "--help") {
            show_usage(argv[0]);
            return 0;
        } 
        else if (arg == "txt") {
            {
                std::ofstream ofs(fileName);
                ofs.exceptions(std::ofstream::badbit);
                try {
                boost::archive::text_oarchive oa(ofs);
                oa & b1;
                }
                catch (std::ofstream::failure e) {
                    std::cerr << "Exception writing to file\n";
                }
            }
            std::cout << "saved to text" << std::endl;
            return 0;
        }
        else if (arg == "bin") {
            {
                std::ofstream ofs(fileName);
                ofs.exceptions(std::ofstream::badbit);
                try {                
                boost::archive::binary_oarchive oa(ofs);
                oa & b1;
                }
                catch (std::ofstream::failure e) {
                    std::cerr << "Exception writing to file\n";
                }
            }
            std::cout << "saved to binary" << std::endl;
            return 0;
        }
        else {
            show_usage(argv[0]);
            return 1;
        }

    }
    return 0;
}
