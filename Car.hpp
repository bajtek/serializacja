// Car.hpp
#ifndef CAR_H_
#define CAR_H_
#include <array>
#include <iostream>

enum class FuelType {DIESEL, GASOLINE};
enum class Colour {RED, GREEEN, BLUE};
enum class EmissionsStandars {EU, CHINA, US};
struct Wheels 
{
    char type[20];
};

class Car{
    public:
    Car (){};
    Car (FuelType ft, )
    // / constructor + getters
    protected:
    FuelType fuel_type;
    std::array<Wheels, 4> wheels;
    uint32_t max_speed;
    Colour colour;
};

class PersonalCar: public Car {
    public:
    // / constructor + getters
    protected:
    uint8_t number_of_places;
    EmissionsStandars emission;
};

#endif